import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import store from '../store';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect: '/login',
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: { requiresAuth: false },
    },
    {
        path: '/panel',
        name: 'Panel',
        component: () => import(/* webpackChunkName: "Panel" */ '../views/Panel.vue'),
        redirect: '/panel/dashboard',
        meta: { requiresAuth: true },
        children: [
            {
                path: '/panel/dashboard',
                name: 'Panel.dashboard',
                component: () => import(/* webpackChunkName: "Panel.dashboard" */ '../views/Paneles/Dashboard.vue'),
                meta: { requiresAuth: true },
            },
            {
                path: '/panel/eventos',
                name: 'Panel.eventos',
                component: () => import(/* webpackChunkName: "Panel.eventos" */ '../views/Paneles/Eventos.vue'),
                meta: { requiresAuth: true },
            },
            {
                path: '/panel/eventos/nuevo',
                name: 'Panel.eventos.nuevo',
                component: () =>
                    import(/* webpackChunkName: "Panel.eventos.nuevo" */ '../views/Eventos/NuevoEvento.vue'),
                meta: { requiresAuth: true },
            },
            {
                path: '/panel/reportes',
                name: 'Panel.reportes',
                component: () => import(/* webpackChunkName: "Panel.reportes" */ '../views/Paneles/Reportes.vue'),
                meta: { requiresAuth: true },
            },
            {
                path: '/panel/configuracion/usuarios',
                name: 'Panel.usuarios',
                component: () =>
                    import(
                        /* webpackChunkName: "Panel.configuracion.usuarios" */ '../views/Configuracion/Usuarios.vue'
                    ),
                meta: { requiresAuth: true },
            },
            {
                path: '/panel/configuracion/nuevo-usuario',
                name: 'Panel.nuevoUsuario',
                component: () =>
                    import(
                        /* webpackChunkName: "Panel.configuracion.usuarios" */ '../views/Configuracion/NuevoUsuario.vue'
                    ),
                meta: { requiresAuth: true },
            },
            {
                path: '/panel/configuracion/personas',
                name: 'Panel.configuracion.personas',
                component: () =>
                    import(
                        /* webpackChunkName: "Panel.configuracion.personas" */ '../views/Configuracion/Personas.vue'
                    ),
                meta: { requiresAuth: true },
            },
            {
                path: '/panel/configuracion/nueva-persona',
                name: 'Panel.nuevaPersona',
                component: () =>
                    import(
                        /* webpackChunkName: "Panel.configuracion.nuevaPersona" */ '../views/Configuracion/NuevaPersona.vue'
                    ),
                meta: { requiresAuth: true },
            },
            {
                path: '/panel/configuracion/direcciones',
                name: 'Panel.configuracion.direcciones',
                component: () =>
                    import(
                        /* webpackChunkName: "Panel.configuracion.direcciones" */ '../views/Configuracion/Direcciones.vue'
                    ),
                meta: { requiresAuth: true },
            },
            {
                path: '/panel/configuracion/nueva-direccion',
                name: 'Panel.configuracion.direcciones.nueva',
                component: () =>
                    import(
                        /* webpackChunkName: "Panel.configuracion.direcciones.nueva" */ '../views/Configuracion/NuevaDireccion.vue'
                    ),
                meta: { requiresAuth: true },
            },
        ],
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach(async (to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        // need to login
        if (store.state.usuario) {
            next();
        } else {
            const usuario = await store.dispatch('getCredencialesLocalStorage');

            if (usuario) {
                next();
            } else {
                next({
                    name: 'Login',
                });
            }
        }
    } else if (to.name == 'Login' && !store.state.usuario) {
        const usuario = await store.dispatch('getCredencialesLocalStorage');
        if (usuario) {
            next({
                name: 'Panel',
            });
        } else {
            next();
        }
    } else {
        next({
            name: 'Panel',
        });
    }
});

export default router;
