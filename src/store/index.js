import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import router from '../router';

// axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        color: 'grey darken-4',
        urlBase: 'http://app-policia.test/api/auth',
        usuario: null,
        credenciales: {
            access_token: '',
            token_type: '',
        },
        config: {
            show:false,
            color:'',
            message:''
        },
        // Catalogos
        cat_usuarios: [],
        cat_personas: [],
        cat_eventos: [],
        cat_paises: [],
        cat_direcciones: [],
        cat_parentescos: [],
        cat_tipos_violencias: [],
        cat_estado_civil: [],
        cat_nivel_academico: [],
        cat_localidades: [],
        cat_municipios: [],
        cat_entidades: [],
        cat_colonias: [],
        cat_autoridades: [],
        cat_instancias: [],
    },
    mutations: {
        SET_USUARIO(state, usuario) {
            localStorage.setItem('usuario', JSON.stringify(usuario));
            state.usuario = usuario;
        },
        SET_CREDENCIALES(state, credenciales) {
            localStorage.setItem('access_token', credenciales.access_token);
            localStorage.setItem('token_type', credenciales.token_type);
            state.credenciales.access_token = credenciales.access_token;
            state.credenciales.token_type = credenciales.token_type;
        },
        SET_NOTIFICACION(state,payload) {
          state.config.show = payload.show;
          state.config.color = payload.color;
          state.config.message= payload.message;
        },
        // CATALOGO SISTEMA
        SET_CATALOGO_USUARIOS(state, payload) {
            state.cat_usuarios = payload;
        },
        SET_CATALOGO_PERSONAS(state, payload) {
            state.cat_personas = payload;
        },
        SET_CATALOGO_DIRECCIONES(state, payload) {
            state.cat_direcciones = payload;
        },
        // CATALOGOS GENERALES
        SET_CATALOGO_VIOLENCIAS(state, payload) {
            state.cat_tipos_violencias = payload;
        },
        SET_CATALOGO_PARENTESCOS(state, payload) {
            state.cat_parentescos = payload;
        },
        SET_CATALOGO_COLONIAS(state, payload) {
            state.cat_colonias = payload;
        },
        SET_CATALOGO_ENTIDADES(state, payload) {
            state.cat_entidades = payload;
        },
        SET_CATALOGO_LOCALIDADES(state, payload) {
            state.cat_localidades = payload;
        },
        SET_CATALOGO_MUNICIPIOS(state, payload) {
            state.cat_municipios = payload;
        },
        SET_CATALOGO_PAISES(state, payload) {
            state.cat_paises = payload;
        },
        SET_CATALOGO_NIVEL_ACADEMICO(state, payload) {
            state.cat_nivel_academico = payload;
        },
        SET_CATALOGO_ESTADO_CIVIL(state, payload) {
            state.cat_estado_civil = payload;
        },
        SET_CATALOGO_AUTORIDADES(state, payload) {
            state.cat_autoridades = payload;
        },
        SET_CATALOGO_INSTANCIAS(state, payload) {
            state.cat_instancias = payload;
        },
    },
    actions: {
        getIniciarSesion({commit, state}, usuario) {
            axios
                .post(`${state.urlBase}/login`, usuario)
                .then((result) => {
                    const {access_token, token_type} = result.data;
                    commit('SET_USUARIO', result.data.usuario);
                    commit('SET_CREDENCIALES', {access_token, token_type});
                })
                .then(() => {
                    router.push('panel');
                });
        },
        async getCerrarSesion({commit, state}) {
            await axios
                .get(`${state.urlBase}/logout`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then(() => {
                    localStorage.clear();
                });

            return true;
        },
        getCredencialesLocalStorage({commit}) {
            const access_token = localStorage.getItem('access_token');
            const token_type = localStorage.getItem('token_type');
            const usuario = JSON.parse(localStorage.getItem('usuario'));

            commit('SET_CREDENCIALES', {access_token, token_type});
            commit('SET_USUARIO', usuario);

            return usuario;
        },
        getRegistrarUsuario({commit, state}, form) {
            axios
                .post(`${state.urlBase}/registrar-usuario`, form, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_USUARIOS', result.data.usuarios);
                    commit('SET_NOTIFICACION',{show:true,color:'success',message: result.data.message})

                });
            return true;
        },
        getRegistrarPersona({commit, state}, form) {
            axios
                .post(`${state.urlBase}/registrar-persona`, form, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_PERSONAS', result.data.personas);
                    commit('SET_NOTIFICACION',{show:true,color:'success',message: result.data.message})

                });
            return true;
        },
        getRegistrarDireccion({commit, state}, form) {
            return axios
                .post(`${state.urlBase}/registrar-direccion`, form, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_DIRECCIONES', result.data.direcciones);
                    commit('SET_NOTIFICACION',{show:true,color:'success',message: result.data.message})
                    return true;
                });
        },
        // SISTEMA
        getListaUsuarios({commit, state}) {
            axios
                .get(`${state.urlBase}/lista-usuarios`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_USUARIOS', result.data);
                });
        },
        getListaPersonas({commit, state}) {
            axios
                .get(`${state.urlBase}/lista-personas`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_PERSONAS', result.data);
                });
        },
        getListaDirecciones({commit, state}) {
            axios
                .get(`${state.urlBase}/lista-direcciones`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_DIRECCIONES', result.data);
                });
        },
        // CATALOGOS
        getParentescos({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-parentescos`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_PARENTESCOS', result.data);
                });
        },
        getTiposViolencia({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-tipos-violencia`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_VIOLENCIAS', result.data);
                });
        },
        getEstadoCivil({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-estados-civiles`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_ESTADO_CIVIL', result.data);
                });
        },
        getNivelAcademico({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-niveles-academicos`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_NIVEL_ACADEMICO', result.data);
                });
        },
        getEntidades({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-entidades`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_ENTIDADES', result.data);
                });
        },
        getMunicipios({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-municipios`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_MUNICIPIOS', result.data);
                });
        },
        getLocalidades({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-localidades`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_LOCALIDADES', result.data);
                });
        },
        getPaises({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-paises`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_PAISES', result.data);
                });
        },
        getAutoridades({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-autoridades`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_AUTORIDADES', result.data);
                });
        },
        getInstancias({commit, state}) {
            axios
                .get(`${state.urlBase}/obtener-instancias`, {
                    headers: {
                        Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                    },
                })
                .then((result) => {
                    commit('SET_CATALOGO_INSTANCIAS', result.data);
                });
        },
        // Busquedas
        getColoniasCP({commit, state}, cp) {
            axios
                .post(
                    `${state.urlBase}/obtener-colonias`,
                    {codigo_postal: cp},
                    {
                        headers: {
                            Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                        },
                    }
                )
                .then((result) => {
                    commit('SET_CATALOGO_COLONIAS', result.data);
                });
        },
        getEML({commit, state}, colonia_id) {
            return axios
                .post(
                    `${state.urlBase}/obtener-eml`,
                    {colonia_id: colonia_id},
                    {
                        headers: {
                            Authorization: `${state.credenciales.token_type} ${state.credenciales.access_token}`,
                        },
                    }
                )
                .then((result) => {
                    return result.data;
                });
        },
    },
    modules: {},
});
